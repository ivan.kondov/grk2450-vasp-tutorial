""" simple computational chemistry interfaces using ASE """
import numpy as np
from ase import Atoms
from ase.io import read, write
from ase.calculators.calculator import get_calculator_class
from ase.constraints import dict2constraint
from ase.neb import NEB
from ase.optimize.bfgs import BFGS
import copy

def cleanup_atoms_dict(adict):
    """ delete keywords in dictionary not allowed in Atoms.__init__ """

    dellist = ['unique_id', 'magmom', 'calculator', 'calculator_parameters',
               'energy', 'forces', 'dipole', 'stress', 'free_energy',
               'charges']
    subdict = {'initial_magmoms': 'magmoms'}
    for key in subdict:
        if key in adict:
            adict[subdict[key]] = adict.pop(key)
    for key in dellist:
        if key in adict:
            del adict[key]
    return adict

@classmethod
def from_dict(cls, adict):
    """ deserialize json serializable dictionaries to atoms objects """
    from ase.io import jsonio    
    from ase.constraints import dict2constraint

    adict_ = jsonio.decode(jsonio.encode(cleanup_atoms_dict(adict)))
    if 'constraints' in adict_:
        constr = adict_.pop('constraints')
        if isinstance(constr, list):
            adict_['constraint'] = [dict2constraint(c) for c in constr]
        else:
            adict_['constraint'] = [dict2constraint(constr)]

    return cls(**adict_)

Atoms.from_dict = from_dict

def atoms2dict(atoms):
    """ convert Atoms object to a dictionary used to recreate the object """
    from ase.io import jsonio
    from ase.db.row import atoms2dict
    import json

    adict = cleanup_atoms_dict(atoms2dict(atoms))
    return json.loads(jsonio.encode(adict))

def read_structure(filename):
    return atoms2dict(read(filename))

def calculation(atoms_dict, calc_dict):
    atoms = Atoms.from_dict(atoms_dict)
    calc_class = get_calculator_class(calc_dict['name'])
    calc = calc_class(**calc_dict['parameters'])
    atoms.set_calculator(calc)
    calc.calculate(atoms)
    energy = atoms.get_potential_energy()
    forces = atoms.get_forces()
    return atoms2dict(atoms), energy, forces

def do_neb(atoms_start_dict, atoms_end_dict, calc_dict):
    initial = Atoms.from_dict(atoms_start_dict)
    final = Atoms.from_dict(atoms_end_dict)
    images = [initial]
    images += [initial.copy() for i in range(5)]
    images += [final]
    calc_class = get_calculator_class(calc_dict['name'])
    calc = calc_class(**calc_dict['parameters'])
    for image in images:
        image.calc = copy.deepcopy(calc)
    neb = NEB(images, climb=False, method='improvedtangent')
    neb.interpolate('idpp')
    write('interpolation.traj', neb.iterimages())
    qn = BFGS(neb, trajectory='pre-neb.traj',logfile='pre-neb.log',)
    qn.run(fmax=0.5)
    neb.climb=True
    qn = BFGS(neb, trajectory='neb.traj',logfile='neb.log',)
    qn.run(fmax=0.1)
    return 
