# VASP Hands-on Tutorial 
This repository contains the hands-on tutorial on the Vienna Ab initio Simulation Package (VASP) [1-4] for the PhD students in the graduate school GRK 2450. Please start this tutorial with the [setup](docs/setup.md).

# References
1. Kresse, G.; Hafner, J. Ab initio molecular dynamics for liquid metals. Phys. Rev. B 1993, 47 (1), 558−561. https://doi.org/10.1103/PhysRevB.47.558
2. Kresse, G.; Hafner, J. Ab initio molecular-dynamics simulation of the liquid-metal amorphous-semiconductor transition in germanium. Phys. Rev. B 1994, 49 (20), 14251−14269. https://doi.org/10.1103/PhysRevB.49.14251
3. Kresse, G.; Furthmüller, J. Efficiency of ab initio total energy calculations for metals and semiconductors using a plane-wave basis set. Comput. Mater. Sci. 1996, 6 (1), 15−50. https://doi.org/10.1016/0927-0256(96)00008-0
4. Kresse, G.; Furthmüller, J. Efficient iterative schemes for ab initio total-energy calculations using a plane-wave basis set. Phys. Rev. B 1996, 54 (16), 11169−11186. https://doi.org/10.1103/PhysRevB.54.11169
