# Running jobs

To run jobs using the batch system the ``qlaunch`` command can be used.

## Deamon mode of ``qlaunch`` combined with ``rlaunch`` with a single firework

In this case ``qlaunch`` will not return but run infinitely check every 60 seconds for fireworks READY for execution. If there is a READY firework a SLURM job will be submitted. The job will execute exactly one firework and exit.

```bash
cd $HOME/grk2450-vasp-tutorial/launches
qlaunch -d 60 -w ../config/fworker_vasp.yaml -q ../config/qadapter_vasp_single.yaml singleshot > qlaunch.log 2>&1 &
```

## Single job submitted with ``qlaunch`` combined with ``rlaunch`` with multiple fireworks

In this case ``qlaunch`` will submit one SLURM job and return. The job will check every 60 seconds for fireworks READY for execution in an infinite loop. If there is any READY firework it will be executed.

```bash
cd $HOME/grk2450-vasp-tutorial/launches
qlaunch -w ../config/fworker_vasp.yaml -q ../config/qadapter_vasp_multiple.yaml singleshot
```

Please note that the path ``$HOME/grk2450-vasp-tutorial/launches`` must be accessible for the other participants in order to share the results with them. If it cannot be made accessible, then you can use a workspace (see below) as storage for the launch directories.


# Running jobs and sharing the results with other participants

The methods above can be used in a workspace that can be easily shared with other participants during the tutorial.

## Set up a workspace

Let us create a workspace that will expire after 15 days and provide user ab1234 access permissions to this work space:

```bash
ws_allocate grk2450-vasp-tutorial 15 # this will return a workspace path
cd <workspace path>
setfacl -m u:ab1234:rX .
```


## Use the workspace to submit jobs

```bash
ws_list # return a list of available workspaces
cd <path of the relevant workspace>
tutorial=$HOME/grk2450-vasp-tutorial
qlaunch -w $tutorial/config/fworker_vasp.yaml -q $tutorial/config/qadapter_vasp_multiple.yaml singleshot
```
