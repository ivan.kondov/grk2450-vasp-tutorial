# Setup

For this tutorial you first have to login on [bwUniCluster](https://wiki.bwhpc.de/e/Category:BwUniCluster_2.0).

## Install the tutorial

The tutorial is available in the KIT Gitlab repository and can be installed with the commands:

```bash
cd $HOME
git clone https://git.scc.kit.edu/jk7683/grk2450-vasp-tutorial.git
```


## Set up the tutorial

```bash
tutorial=$HOME/grk2450-vasp-tutorial
export PYTHONPATH=$tutorial/lib:$PYTHONPATH
export PATH=$tutorial/bin:$PATH
```

## Python


### Python virtual environment

It is strongly recommended to set up a python virtual environment to avoid modifying your current python installation or, if you use an installation for which you do not have write permissions, to avoid access problems. Now, you should set up the virtual environment with the commands

```bash
cd $HOME/grk2450-vasp-tutorial
python -m venv python-3.6.8
```

After this you activate the virtual environment and update the ``pip`` program:

```bash
. python-3.6.8/bin/activate
python -m pip install --upgrade pip
```


### Install Python packages

The following python packages must be installed:

```bash
python -m pip install ase
python -m pip install fireworks
python -m pip install pyaml
python -m pip install prettytable
```


## Setting up a FireServer

The tutorial requires a FireServer which is accessible from your computer and
from your computing resource (e.g. an HPC cluster). The connection to the FireServer is configured in the launchpad configuration file named ``launchpad.yaml`` in the folder ``$HOME/.fireworks`` (this folder must be created if necessary). The file must have the following contents:

```yaml
host: <hostname of the FireServer>
port: 27017
name: <database name>
username: <your username>
password: <your password>
ssl: true
ssl_ca_certs: <path to root CA certificate>
ssl_certfile: <path to client certificate>
```

The full paths to the certificate files must be configured in the launchpad file. You will get the access data from the instructors. Then, in the folder ``$HOME/.fireworks``, the fireworks configuration file ``FW_config.yaml`` must be created with the following command:

```bash
echo LAUNCHPAD_LOC: $HOME/.fireworks/launchpad.yaml >> $HOME/.fireworks/FW_config.yaml
```

This configuration has to be made available on all resources that will use the
FireServer, i.e. your local computer, login nodes and compute nodes of the
clusters etc. If these resources share the same ``$HOME`` file system then the
configuration has to be done only once.


## Testing the setup

Everything should have been configured correctly if this command returns the current number of workflows in the database:

```bash
lpad get_wflows -d count
```

If the test works then you can go on with [creating and submitting calculations](basics.md).
