# Basic procedures

## Prepare inputs

The inputs necessary to start a job are a structure file and a calculator file.

### Structure file

The inputs structure can be provided in any [format](https://wiki.fysik.dtu.dk/ase/ase/io/io.html) supported by ASE. In this tutorial it is recommended to use the POSCAR file format. Any program can be used to prepare the structure but here the [ASE's GUI](https://wiki.fysik.dtu.dk/ase/ase/gui/gui.html) can be recommended.

### Calculator file

This is a file in JSON or YAML format that contains the name and the parameters of the ASE calculator. The name in this tutorial is *vasp* and the parameters keywords are the same as if provided in the ``INCAR`` file. Additional keywords cover the contents of the ``KPOINTS`` file and direct the selection of the proper pseudopotentials (``POTCAR``). For more information it is referred to the [Vasp calculator documentation](https://wiki.fysik.dtu.dk/ase/ase/calculators/vasp.html).

## Submit jobs

### Recommended approach

The script ``job_submit.py`` can be used to submit jobs with a command like this:

```bash
job_submit.py -s structure.poscar -c params.yaml -p "Jane Doe"
```

A short help about the required and acceptable arguments of ``job_submit.py`` can be obtained with:

```bash
job_submit.py --help
```

In order to distinguish jobs of different exercises the ``-n`` flag can be used, for example:

```bash
job_submit.py -s structure.poscar -c params.yaml -p "Jane Doe" -n "C adsorption"
```

### Advanced approach

The job can be submitted as a fireworks workflow in JSON or YAML format. This input must be prepared in advance using the template file. The calculator parameters and the structure must be included into it. The structure must be serialized into JSON or YAML first by using the provided script ``structure2yaml.py``.

After the input is prepared it can be submitted using the script ``job_submit.sh``. For example, if the input
is in the file ``workflow.yaml``:

```bash
job_submit.sh workflow.yaml
```

## Query job status and getting job results

To list all jobs of participant Jane Doe, the ``job_list.py`` script can be used like this:

```bash
job_list.py -p "Jane Doe"
```

All inputs and outputs of all jobs of participant Jane Doe can be dumped with ``job_dump.py``:

```bash
job_dump.py -p "Jane Doe"
```

Inputs and outputs of specific jobs can be dumped using their firework IDs, e.g.:

```bash
job_dump.py -i 2 6 7
```

will dump the jobs with firework IDs 2, 6 and 7. The firework IDs can be found
from the command ``job_list.py``. More options of job_dump.py can be found
with the ``--help`` flag. The command ``job_dump.py`` will create the files
``<fw_id>.yaml`` and ``<fw_id>.tgz`` for every firework ID matching the query.
The YAML file ``<fw_id>.yaml`` contains a dump of the fireworks workflow from
the Launchpad. The file ``<fw_id>.tgz`` contains an archive of the launch
directory of the job.
