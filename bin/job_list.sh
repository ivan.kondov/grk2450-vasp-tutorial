#!/bin/bash -eu

if [ $# -eq 0 ]; then
    echo "error: participant name must be provided as first argument"
    exit 1
fi

name="$1"
query=\{\"metadata.participant\":\"$name\"\}
lpad get_wflows -q "$query" -t
