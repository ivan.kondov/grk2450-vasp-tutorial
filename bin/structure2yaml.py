#!/usr/bin/env python
""" prints Atoms yaml from a structure file in any format """

import sys, yaml
from compchem import read_structure

if len(sys.argv) < 2:
    raise RuntimeError('missing input filename', sys.argv[0])
print(yaml.dump(read_structure(sys.argv[1])))
