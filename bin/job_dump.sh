#!/bin/bash -eu

# This script collects the data from the launch directory and launchpad
# for all jobs matching the query and creates a tar archive and a YAML dump

if [ $# -eq 0 ]; then
    echo "error: participant name must be provided as first argument"
    exit 1
fi

if [ $# -gt 1 ]; then
    job_name=$2
    query=\{\"metadata.participant\":\"$1\",\"name\":\"$2\"\}
else
    query=\{\"metadata.participant\":\"$1\"\}
fi

fw_ids=$(lpad get_wflows -q "$query" -d ids | tr -d '[],')

for fw_id in ${fw_ids}; do
    lpad -o yaml dump_wflow -i ${fw_id} -f fw_${fw_id}.yaml
    ldir=$(lpad get_launchdir ${fw_id})
    tar -czf fw_${fw_id}.tgz -C $(dirname ${ldir}) $(basename ${ldir})
done
