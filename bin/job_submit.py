#!/usr/bin/env python3
""" This script submits single computational chemistry jobs as fireworks """

import yaml
from argparse import ArgumentParser
from fireworks import Firework, Workflow, PyTask, ScriptTask, FileDeleteTask
from fireworks.user_objects.dupefinders.dupefinder_exact import DupeFinderExact
# from fireworks.utilities.dagflow import DAGFlow
from fireworks.scripts.lpad_run import get_lp
from fireworks.fw_config import CONFIG_FILE_DIR
from compchem import read_structure


def get_args():
    """ parse command-line parameters """
    m_description = 'Submit a single computational chemistry job'
    parser = ArgumentParser(description=m_description)
    parser.add_argument('--config_dir',
                        help='path to configuration file (if -l unspecified)',
                        default=CONFIG_FILE_DIR)
    parser.add_argument('-l', '--launchpad_file',
                        help='path to launchpad file', default=None)
    parser.add_argument('-s', '--structure_file', required=True,
                        help='path to structure file')
    parser.add_argument('--neb', required=False,
                        help='path to final structure file for NEB')
    parser.add_argument('-c', '--calculator_file', required=True,
                        help='path to parameter file')
    parser.add_argument('-p', '--participant', required=True,
                        help='participant name', default=None)
    parser.add_argument('-n', '--job_name', required=False,
                        help='job name', default='grk2450-vasp')
    parser.add_argument('-o', '--output_file', required=False,
                        help='path to workflow output file', default=None)
    return parser.parse_args()

if __name__ == '__main__':
    args = get_args()
    tasks = []
    if not args.neb:
        tasks.append(PyTask(func='compchem.calculation', inputs=['input structure',
                     'calculator'], outputs=['output structure', 'energy',
                     'forces']))
    else:
        tasks.append(PyTask(func='compchem.do_neb', inputs=['input structure',
                     'final input structure', 'calculator'], outputs=[]))
    tasks.append(FileDeleteTask(files_to_delete=['POTCAR']))
    tasks.append(ScriptTask(script=r'chmod -R og+rX .'))

    atoms_dict = read_structure(args.structure_file)
    atoms_dict_final = read_structure(args.neb) if args.neb else None
    with open(args.calculator_file, 'r') as yf:
        calc_dict = yaml.safe_load(yf)

    fw_spec = {'_category': 'vasp',
               'input structure': atoms_dict,
               'final input structure': atoms_dict_final, 'calculator': calc_dict}
    fw_name = ' '.join([args.job_name, 'firework'])
    fw_1 = Firework(tasks=tasks, spec=fw_spec, name=fw_name)

    wf_meta = {'participant': args.participant}
    workflow = Workflow(fireworks=[fw_1], links_dict={fw_1: []},
                        name=args.job_name, metadata=wf_meta)
    # DAGFlow.from_fireworks(workflow).check()

    if args.output_file:
        workflow.to_file(args.output_file)

    launchpad = get_lp(args)
    launchpad.add_wf(workflow)
