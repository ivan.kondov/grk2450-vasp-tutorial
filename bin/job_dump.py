#!/usr/bin/env python3
"""This script collects the data from the launch directory and launchpad
   for all jobs matching the query and creates a tar archive and a YAML dump"""

import os
import shutil
import tarfile
from argparse import ArgumentParser
from fireworks.scripts.lpad_run import get_lp
from fireworks.fw_config import CONFIG_FILE_DIR


def get_args():
    """ parse command-line parameters """
    m_description = 'Dump the outputs of computational chemistry jobs'
    parser = ArgumentParser(description=m_description)
    parser.add_argument('--config_dir',
                        help='path to configuration file (if -l unspecified)',
                        default=CONFIG_FILE_DIR)
    parser.add_argument('-l', '--launchpad_file',
                        help='path to launchpad file', default=None)
    parser.add_argument('-i', '--fw_id', required=False, default=None,
                        help='firework ids', nargs='+', type=int)
    parser.add_argument('-p', '--participant', required=False,
                        help='participant name', default=None)
    parser.add_argument('-n', '--name', required=False,
                        help='job name', default=None)
    return parser.parse_args()


if __name__ == '__main__':
    args = get_args()
    lp = get_lp(args)

    args_error = ('either participant or job name or firework ids must be '
                  'specified')
    assert args.participant or args.name or args.fw_id, args_error

    if args.fw_id:
        fw_ids = args.fw_id
    else:
        query = {}
        if args.participant:
            query.update({'metadata.participant': args.participant})
        if args.name:
            query.update({'name': args.name})
        fw_ids = lp.get_wf_ids(query)

    for fw_id in fw_ids:
        lp.get_wf_by_fw_id(fw_id).to_file(str(fw_id)+'.yaml')
        ld = lp.get_launchdir(fw_id)
        if ld is not None and os.path.exists(ld):
            basename = os.path.basename(ld)
            if 'POTCAR' in os.listdir(path=ld):
                ldmine = os.path.join('/tmp', basename)
                shutil.copytree(ld, ldmine)
                os.remove(os.path.join(ldmine, 'POTCAR'))
                with tarfile.open(name=str(fw_id)+'.tgz', mode='w|gz') as tf:
                    tf.add(ldmine, basename)
                shutil.rmtree(ldmine)
            else:
                with tarfile.open(name=str(fw_id)+'.tgz', mode='w|gz') as tf:
                    tf.add(ld, basename)
