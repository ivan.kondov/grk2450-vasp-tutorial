#!/usr/bin/env python3
"""This script lists all jobs of a give participant"""

from argparse import ArgumentParser
from fireworks.scripts.lpad_run import get_wfs, get_output_func
from fireworks.fw_config import CONFIG_FILE_DIR


def get_args():
    """ parse command-line parameters """
    m_description = 'List computational chemistry jobs'
    parser = ArgumentParser(description=m_description)
    parser.add_argument('--config_dir',
                        help='path to configuration file (if -l unspecified)',
                        default=CONFIG_FILE_DIR)
    parser.add_argument('-l', '--launchpad_file',
                        help='path to launchpad file', default=None)
    parser.add_argument('-p', '--participant', required=False,
                        help='participant name', default=None)
    parser.add_argument('-n', '--name', required=False,
                        help='job name', default=None)
    parser.add_argument('-s', '--state', required=False,
                        help='job state', default=None)
    parser.add_argument('-i', '--fw_id', required=False, default=None,
                        help='firework ids', nargs='+', type=int)
    parser.add_argument('-o', '--output', choices=['json', 'yaml', 'table'],
                        default='table', type=lambda s: s.lower(),
                        help='Output format: json, yaml or table (default)')
    return parser.parse_args()


if __name__ == '__main__':
    args = get_args()

    args_error = ('either participant or job name or firework ids must be '
                  'specified')
    assert args.participant or args.name or args.fw_id, args_error

    if args.participant:
        args.query = '{"metadata.participant": "'+str(args.participant)+'"}'
    else:
        args.query = None
    if args.output == 'table':
        args.table = True
    else:
        args.table = False
        args.output = get_output_func(args.output)
    args.display_format = 'less'
    args.sort = None
    args.rsort = None
    args.max = None
    get_wfs(args)
