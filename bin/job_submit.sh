#!/bin/bash -eu
# This script submits single computational chemistry jobs as fireworks
# The input must be a fireworks workflow in YAML or JSON format
# The input is specified as an argument
lpad add $1
